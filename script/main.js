// 1) DOM це обєктна модель документа. Кожен HTML-тег є об’єктом. Вкладені теги – це “діти” всередині батьківського елементу. Текст всередині тегу також є об’єктом.
//Всі ці об’єкти доступні за допомогою JavaScript, і ми можемо використовувати їх, щоб змінити сторінку.
// 2) innerText повертає тільки текст,синтаксис html він не покаже. innerHTML покаже всю інформацію разом із тегами
// 3) Найкраще звертатись до елемента за допомогою querySelector або  querySelectorAll


let paragraph = document.querySelectorAll('p');

paragraph.forEach(el => el.style.backgroundColor = 'red');

const list = document.querySelector('#optionsList');
console.log(list);

let parent = list.parentElement;
console.log(parent);

if (list.hasChildNodes()) {
    let nodes = list.children;
    for (let i = 0; i < nodes.length; i++) {
        console.log("Дочірня нода зназвою " + nodes[i].nodeName + " і типом " + nodes[i].nodeType);
    }
}

let newParagraph = document.querySelector('#testParagraph');
newParagraph.textContent = 'This is a paragraph';
console.log(newParagraph);

let mainHeader = document.querySelector('.main-header');
let child = mainHeader.children;
console.log(child);

for (let i = 0; i < child.length; i++) {
    child[i].classList.add('nav-item')
}

let sectionTitle = document.querySelectorAll('.section-title');


sectionTitle.forEach(el => el.classList.remove('section-title'));
console.log(sectionTitle);
